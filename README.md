

# Craft CMS 3 Boilerplate
[craft.whitespacers.com](http://craft.whitespacers.com). Hosted on riker.

Admin: [craft.whitespacers.com/admin](http://craft.whitespacers.com/admin)
Usernanme: whitespace
Password: development

This repo contains templates required for creating a basic site with Craft 3. It allows you to create the following:

* a Homepage
* a Structured main nav section where you can create as many standard content pages and sub pages as you wish
* a Structured footer section where you can create as many standard content pages as you wish
* News items

A fully functional search is also in place, which can be accessed at `/search`.

## Requirements
The following plugins are required (all of which are free):

* [Redactor]([https://plugins.craftcms.com/redactor](https://plugins.craftcms.com/redactor))
* [SEO]([https://plugins.craftcms.com/seo](https://plugins.craftcms.com/seo))
* [Section Field]([https://plugins.craftcms.com/section-field](https://plugins.craftcms.com/section-field))

Although not required, it is strongly recommended that you install the [Child Me]([https://plugins.craftcms.com/child-me](https://plugins.craftcms.com/child-me)) plugin. This will assist with content authoring.

## Sections
The following Sections are required to be setup:

|Name |Type |
| --- | :--- | :--- | :---
| Home | Single
| Main | Structure
| Footer | Structure
| News | Channel

### Sections Configuration
Please login to the admin to view the Sections configuration. These are configured specifically to work with the templates in this repo.

## Fields
The following Fields are required to be setup:

|Name |Type |
| --- | :--- | :--- | :---
| Main Content | Matrix
| SEO | SEO


### Fields Configuration
Please login to the admin to view the Fields configuration. These are configured specifically to work with the Sections and templates.

The Main Content Matrix field is configured to be used across all pages on the site. It allows content authors to build and rearrange content in a modular way. Think of the blocks in this field as Lego blocks that can be arranged in anyway the author sees fit.

Whenever you create a new block within the Main Content Matrix field, you must create a corresponding template in `/craft/templates/includes/components`.

## File structure

```
├── ./
├── craft/
│   ├── templates/
│   	├──  home/ [1]
│   	├──  includes/ [2]
│   		├──  components/ [3]
│   	├──  layouts/ [4]
│   	├──  news/ [5]
│   	├──  pages/
│   		├──  types/ [6]
│   	├──  404.twig [7]
│   	├──  search.twig [8]

```

* [1] - contains the template for the Homepage
* [2]  - various global include templates such as header and footer content
* [3] - the templates for each of the Main Content Matrix blocks
* [4] - contains the main Layout template
* [5] - contains the template for News entries
* [6] - contains the templates for each of the different Page types (there is currently only one)
* [7] - 404 tempalte
* [8]  - search template